import OrderSummary from '../components/OrderSummary';
import { useState, useEffect } from 'react';




export default function Checkout(){


	const [myOrder, setMyOrder] = useState([]);

	useEffect(() => {

	fetch('http://localhost:4000/users/myOrders')
	.then(res => res.json())
	.then(data => {
		console.log(data)
	
	setMyOrder(data.map((order) => {
	return (
		<OrderSummary key = {order._id} order = {order} />
		)
	}))


		})
	}, [])

	console.log(myOrder)

	return(

		<>
			{myOrder}
		</>

		);
}