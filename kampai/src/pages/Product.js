import { useState, useEffect } from 'react'
import Cart from '../components/Cart';
import Data from '../Data'
import ItemCard from  '../components/ItemCard';
import Swal from 'sweetalert2'

export default function Product(){

	const [myCart, setMyCart] = useState([]);
	const [myProduct, setProduct] = useState([]);

	const totalPrice = myCart.reduce((a,c) => a + c.price * c.qty, 0);

	function createOrder(){


		fetch('http://localhost:4000/users/myOrder' , {
			
			method: 'POST',
        	headers: {
            		'Content-Type' : 'application/json',
           			 Authorization : `Bearer ${localStorage.getItem("token")}`

          },

          	body: JSON.stringify({
            	cartedOrder: myCart	,
           		 total: totalPrice       	
        	})
         })
        	.then(res => res.json())	
    		.then(data => {
        		console.log(data)

        	if(data){

             Swal.fire({
                title: 'Order has been made!',
                icon: 'success',
                text: 'You can now proceed to checkout'
             })
    
          } else {

              Swal.fire({
                 title: 'Authentication Failed!',
                icon: 'error',
             text: 'Check your login details.'
            })

        }

       })

		setMyCart([]);
			
}



	useEffect(() => {

	fetch('http://localhost:4000/product/all')
	.then(res => res.json())
	.then(data => {
		console.log(data)
	
	setProduct(data.map((product) => {
	return (
		<ItemCard key = {product._id} productProps = {product} myCart={myCart} setMyCart={setMyCart}/>
		)
	}))


		})
	}, [])

	
	

	
	return (
		<>	
			<h1 className="text-center mt-3"> All Products</h1>
				<section className="py-4 container">
					<div className="row justify-content-center">
								{myProduct}												
					</div>
				</section>


		<section>

				{
					(myCart.length === 0) ?  <h5 className="text-center">Cart is Empty</h5>
				
					:

					<div>
					
						<h3>Cart</h3>
				
				
		

						<div>

							{console.log(myCart)}
							{console.log(setMyCart)}
						
							{myCart.map((item, index)=>{
								return(

									<Cart

										img={item.image} 
										title={item.name} 
										price={item.price}
										item={item}
										qty={item.qty}
										key={index}
										myCart={myCart}
										setMyCart={setMyCart}
									/>
									)
							})
							
							}
							

						</div>

						
						<h2>Total Price: Php {totalPrice}</h2>

						<div>
							<button className="btn btn-danger ms-2" onClick={() => setMyCart([])}>Clear cart</button>
							<button className="btn btn-info ms-2" onClick={() => createOrder()}>Check out</button>
						</div>

			
					
					</div>
				}

			</section>

		</>

		);
};