import { useState, useEffect, useContext } from 'react';
import {Form, Button , Container, Col, Row} from 'react-bootstrap'
import { Redirect , useHistory } from 'react-router-dom'
import Swal from 'sweetalert2'
import UserContext from '../UserContext'




export default function Register(){

	const { user, setUser } = useContext(UserContext)

	const history = useHistory()

	const [password1, setPassword1] = useState('')
	const [password2, setPassword2] = useState('')
	const [email, setEmail] = useState('')
	const [firstName, setFirstName] = useState('')
	const [lastName, setLastName] = useState('')
	const [mobileNo, setMobileNo] = useState('')
	const [isActive, setIsActive] = useState(false)

	function register(e){

		e.preventDefault();

		fetch('http://localhost:4000/users/register',{
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				firstName : firstName,
				lastName : lastName,
				email : email,
				mobileNo : mobileNo,
				password : password1
			})

		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data === true){
				Swal.fire({
					title: 'Register Successful',
					icon: 'success',
					text: 'You may now proceed to login!'
				})

				history.push("/login")

			} else {

				Swal.fire({
					title: 'Registration Failed.',
					icon: 'error',
					text: 'Please check details.'
				})

			}
		})
	}

	useEffect(() => {
		if((email !== '' && password1 !== '' && password2 !== '' && firstName !== '' && lastName !== '' && mobileNo !== '') && (password1 === password2)){

			setIsActive(true)
		}else {
			setIsActive(false)
		}
	}, [email, password1, password2])


	return(
				


				<Container className="mt-5" >
					<Row className="justify-content-center">
						<Col className="col-12 col-md-4 col-lg-6 border py-3 px-3 shadow bg-light" >
							<Form onSubmit={(e)=>register(e)}>

								<Form.Group>
									<Form.Label> Firstname:</Form.Label>
									<Form.Control
										type = 'firstName'
										placeholder = 'Please enter your firstname'
										value = {firstName}
										onChange = {e => setFirstName(e.target.value)}
										required
									/>
								</Form.Group>

								<Form.Group>
									<Form.Label> Lastname:</Form.Label>
									<Form.Control
										type = 'lastName'
										placeholder = 'Please enter your lastname'
										value = {lastName}
										onChange = {e => setLastName(e.target.value)}
										required
									/>
								</Form.Group>

								<Form.Group>
									<Form.Label> mobileNo:</Form.Label>
									<Form.Control
										type = 'mobile number'
										placeholder = 'Please enter your mobile number'
										value = {mobileNo}
										onChange = {e => setMobileNo(e.target.value)}
										required
									/>
								</Form.Group>

								<Form.Group>
									<Form.Label> Email Address:</Form.Label>
									<Form.Control
										type = 'email'
										placeholder = 'Please enter your email here'
										value = {email}
										onChange = {e => setEmail(e.target.value)}
										required
									/>
								</Form.Group>

								<Form.Group controlId = "password1">
									<Form.Label>Password:</Form.Label>
									<Form.Control
										type = 'password'
										placeholder = 'Please input your password here'
										value = {password1}
										onChange = {e => setPassword1(e.target.value)}
										required
									/>
								</Form.Group>

								<Form.Group controlId = 'password2'>
									<Form.Label>Verify Password</Form.Label>
									<Form.Control
									type = 'password'
									placeholder = 'Please verify your password'
									value = {password2}
									onChange = {e => setPassword2(e.target.value)}
									required
									/>
								</Form.Group>


							{ isActive ? 
								<Button variant = 'primary' type = 'submit' id = 'submitBtn' className="mt-3">Register</Button>

								:

								<Button variant = 'danger' type = 'submit' id = 'submitBtn' className="mt-3" disabled>Register</Button>
							}
							</Form>
					</Col>
				</Row>
			</Container>

	)
}
