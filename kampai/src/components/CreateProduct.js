import { useState, useEffect, useContext } from 'react';
import {Form, Button} from 'react-bootstrap'
import { Redirect } from 'react-router-dom'
import Swal from 'sweetalert2'
import UserContext from '../UserContext'
import axios from 'axios';


export default function CreateProduct(){


	const { user, setUser } = useContext(UserContext);

	const [name, setName] = useState('');
	const [price, setPrice] = useState('');
	const [description, setDescription] = useState('');
	const [quantity, setQuantity] = useState('');
	const [image, setImage] = useState('');
	const [isActive, setIsActive] = useState(false);


	console.log(image);


function createNew(e){

	e.preventDefault();

	const fd = new FormData();
	fd.append("name", name)
	fd.append("price", price)
	fd.append("description", description)
	fd.append("quantity", quantity)
	fd.append("image", image )


	setName('');
	setPrice('');
	setDescription('');
	setQuantity('');
	setImage('');
	axios.post('http://localhost:4000/product/createNew', fd, { headers: { 
				'Content-Type' : 'application/json',
				Authorization : `Bearer ${localStorage.getItem("token")}`
			}
		})


		.then((res) => {

			console.log(res.data)
			
			if(res.data === true){

				Swal.fire({
					title: 'Created new product',
					icon: 'success',
					text: 'New Product was added to product list'
				})


			} else {

				Swal.fire({
					title: 'Creation of product failed.',
					icon: 'error',
					text: 'Please check details.'
				})

			}
		})
		
	}



	useEffect(() => {
		if(name !== '' && price !== '' && description !== '' && quantity !== '' && image !== '') {
			setIsActive(true)
		
		}else {
			setIsActive(false)
		}
	}, [name, price, description, quantity, image])


	return (

		<>
		
		{

		(user._id !== null) ?
    	
			<Form encType='multipart/form-data' onSubmit={(e)=>createNew(e)}>

			<Form.Group>
				<Form.Label> Name:</Form.Label>
				<Form.Control
					type = 'Name'
					placeholder = 'Please enter name of the product'
					value = {name}
					onChange = {e => setName(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group>
				<Form.Label>Price</Form.Label>
				<Form.Control
					type = 'price'
					placeholder = 'Please enter product price'
					value = {price}
					onChange = {e => setPrice(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group>
				<Form.Label>Description</Form.Label>
				<Form.Control
					type = 'description'
					placeholder = 'Please enter your mobile number'
					value = {description}
					onChange = {e => setDescription(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group>
				<Form.Label>Quantity</Form.Label>
				<Form.Control
					type = 'quantity'
					placeholder = 'Please enter your email here'
					value = {quantity}
					onChange = {e => setQuantity(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group>
				<Form.Label>Image</Form.Label>	
				<Form.Control
					type = 'file'
					filename = 'image'
					onChange = {e => setImage(e.target.files[0])}
					required
				/>
			</Form.Group>

		{ isActive ? 
			<Button variant = 'primary' type = 'submit' id = 'submitBtn' className="mt-2">Create</Button>

			:

			<Button variant = 'danger' type = 'submit' id = 'submitBtn' className="mt-2" disabled>Create</Button>
		}


		</Form>


		:

		<Redirect to="/login" />

		}

		</>
	);

};