import {Card, Button} from 'react-bootstrap';


export default function ItemCard(props){

	const { myCart, setMyCart} = props;

	console.log(props.productProps)
	console.log(props.productProps)

	function addItem(item){
		const isExist = myCart.find((e) => e._id === item._id );
		console.log(isExist)

		if(isExist){
			setMyCart(myCart.map((e)=>  e._id === item._id ? {
				...isExist, qty: isExist.qty + 1
			} : e 
				)
			);
		} else {
					setMyCart(prevMyCart =>[...prevMyCart, {...item, qty: 1}])

		}
	}

	console.log(myCart.length)
	
	
	return(


		<div className="col-11 col-md-6 col-lg-3 mx-0 mb-4">
			<Card className="p-0 overflow-hidden h-60 shadow">
				<Card.Img  variant="top" src={`http://localhost:4000/${props.productProps.image}`} className='img-fluid' />
				<Card.Body className='text-center'>
					<Card.Title>{props.productProps.name}</Card.Title>
					<h5>
							Php {props.productProps.price}
					</h5>					
					<Card.Text>
							{props.productProps.description}
					</Card.Text>

					<Button variant="primary" onClick={() => addItem(props.productProps)}>Add to Cart</Button>
				</Card.Body>
			</Card>			
		</div>

	);
};