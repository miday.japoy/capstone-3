



export default function Cart(props){

	const {myCart, setMyCart} = props;

	console.log(props)


	

	function addQuantity(item){
		

		const isExist = myCart.find((e)=> e._id === item._id);
		console.log(isExist)

		if(isExist){
			setMyCart(myCart.map((e)=>  e._id === item._id ? {
				...isExist, qty: isExist.qty + 1
			} : e 
				)
			);
		} else {
					setMyCart([...myCart, {...item, qty: 1}])

		}
	}

	console.log(props.item)

	function reduceQuantity(item){
		const isExist = myCart.find((e)=> e._id === item._id);
		if(isExist.qty === 1){
			setMyCart(myCart.filter((e)=> e._id !== item._id));
		} else {

			setMyCart(myCart.map((e)=>  e._id === item._id ? {
				...isExist, qty: isExist.qty - 1	
			} : e 
				)
			);
		}
	}


	function removeItem(item){
		
			setMyCart(myCart.filter((e)=> e._id !== item._id));
	}



	return(


		<section className="container">
			<div className="row justify-content-center">
				<div className="col-12">
					<table className="table table-light table-hover">
					<tbody className="mx-0">
						<tr>
							<td>
								<img src={`http://localhost:4000/${props.img}`} style={{ height: '6rem' }} />
							</td>
							<td>
								{props.title}
							</td>
							<td>
								Php {props.price}
							</td>

							<td>
								qty: {props.qty}
							</td>

							<td>
								<button className="btn btn-info ms-2" onClick={()=> addQuantity(props.item)}>+</button>
								<button className="btn btn-info ms-2" onClick={()=> reduceQuantity(props.item)}>-</button>
								<button className="btn btn-danger ms-2" onClick={()=> removeItem(props.item)}>Remove Item</button>
							</td>
						</tr>
					</tbody>
					</table>
				</div>						
			</div>
		</section>

		
		

	);

	
};