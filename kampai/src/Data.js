
import img2 from './img/beer1.jpg'



const Data = [
	
	{
		id: 1,
		img: img2,
		title: 'Gin',
		desc: '70% Alcohol',
		price: 30
	},


	{
		id: 2,
		img: img2,
		title: 'Beer',
		desc: '8% Alcohol',
		price: 50
	},

	{
		id: 3,
		img: img2,
		title: 'Rum',
		desc: '30% Alcohol',
		price: 70
	}

];


export default Data;