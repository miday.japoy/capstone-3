const express = require("express");
const router = express.Router();
const cartController = require("../controller/cart");
const auth = require("../auth");


//Creating order

router.post("/myOrder", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)

	cartController.createOrder(userData, req.body).then(resultFromController => res.send(resultFromController));
})


router.post("/:orderId/checkout", auth. verify, (req,res) => {
	const userData = auth.decode(req.headers.authorization)

	if(userData.isAdmin) {
		return false
	} else {
		let data = {
			userId: userData.id,
			orderId: req.params.orderId
		}

		cartController.checkoutOrder(data).then(resultFromController => res.send(resultFromController));
	}
})


// retrieving all users order

router.get("/myOrders", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)

	cartController.retrieveMyOrders(userData).then(resultFromController => res.send(resultFromController));
})



module.exports = router;
