const express = require("express");
const router = express.Router();
const userController = require("../controller/user");
const auth = require("../auth");


// registration route
router.post("/register", (req, res) => {

	userController.userRegistration(req.body).then(resultFromController => res.send(resultFromController));
})

// Login route
router.post("/login", (req, res) => {
	
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
})


// Admin Functiontonality

router.put("/:userId/setAsAdmin", auth.verify, (req, res) =>{
	const userData = auth.decode(req.headers.authorization)
	
	userController.changeToAdmin(req.params, userData).then(resultFromController => res.send(resultFromController));
})


router.get("/details", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
		userController.getProfile({userId : userData.id}).then(resultFromController => res.send(resultFromController));
	})

module.exports = router;
