const Product = require("../models/product")


// Adding new product
module.exports.addProduct = async (data, reqBody, reqFile) => {

	let newProduct = new Product({

		name: reqBody.name,
		description: reqBody.description,	
		price: reqBody.price,
		quantity: reqBody.quantity,
		image: reqFile.path
	});

	if(data.isAdmin){

		return Product.find({}).then(results => {

			const name = newProduct.name;
			const result = results.find(e => e.name === name);

			if (result === undefined) {

				return newProduct.save().then(product => {
					if(product === null){		
						return false
				} else {
					return true
					}
				})
			}  else {
				return false
				}
		}) 
	}	else {
		return false
		}
}


// Retrieving all product

module.exports.retrieveAllProduct = () => {

	return Product.find({}).then(result => {
		return result
	})
}



// Retrieving single Product

module.exports.retrieveOneProduct = (reqParams) => {

	return Product.findById({_id :reqParams.productId}).then(result => {
		if(result == null){
			return `Product not found.`
		} else {
			return result
		}
	})
}

// Update product

module.exports.updateProduct = async (data, reqParams, reqBody) => {

	if(data.isAdmin){

		let updatedProduct = {
			name: reqBody.name,
			description: reqBody.description,
			price: reqBody.price,
			quantity: reqBody.quantity
		};

		return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then(result => { 

			if(result == null){
				return `Product not found.`
			} else {
				return `Updated ${result.name}.`
			}
		})

	} else {
		return `Access denied.`
		}
}


// Product Archiving


module.exports.archiveProduct = async (data, reqParams) => {	

		if(data.isAdmin){

			return Product.findById({_id: reqParams.productId}).then((result) => {

				if(result == null) {
					return `Product does not exist.`
				} else {
					result.isActive = false;
					return result.save().then(saveResult => {
						return `${result.name} is now on achived.`
					})
			}

		}) 
	
	} else { 

		return `Access denied.`
	}
			
}