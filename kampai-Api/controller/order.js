const Order = require("../models/order");
const User = require("../models/user");
const auth = require("../auth");
const Product = require("../models/product")




module.exports.createOrder = async (data, reqBody) => {

	if(data.isAdmin){

		return `Access Denied.`
	} else {

		let newOrder = new Order({

			totalAmount: reqBody.totalAmount,
			orderDetails: reqBody.orderDetails
		});

		return newOrder.save().then(result => {

			if(result == null){
				return `Something went wrong.`
			} else {
				return `Order has been created. Plesae proceed to checkout.`
			}
		})

		}
	
}


module.exports.checkoutOrder = async (data) => {

	let isUserUpdated = await User.findById({_id: data.userId}).then(foundUser =>{

		foundUser.userOrders.push({orderId: data.orderId});

		return foundUser.save().then(savedUser => {
			if(savedUser == null){
				return false
			} else {
				return true
			}
		})

	})


	let isProductUpdated = await Order.findById({_id: data.orderId}).then(foundOrder =>{
		let orderDetails = foundOrder.orderDetails;

		let product = []; 
		product = orderDetails.map(e => e.productId);

		for(let i = 0; i < product.length; i++){

		 Product.findById({_id: product[i]}).then(foundProduct => { 
				foundProduct.purchases.push({orderId: data.orderId});

				return foundProduct.save().then(savedProduct =>{

					if(savedProduct = null){
						return false
					} else {
						return true
					}
				})	


			})
		} 
	})		


		if(isUserUpdated == true && isProductUpdated == undefined){
			return `Order successful.`
		} else {
			return `Check Details.`
		}


}


// Retrieving all orders

module.exports.retrieveAllOrders = async (data) => {

	if(data.isAdmin){

		return Order.find().then(result => {
			if(result == null){
				return `Something went wrong.`
			} else {
				return result
			}
		})
	} else {
		return `Access Denied.`
	}
}


// retrieve user's orders

module.exports.retrieveMyOrders = async (data) => {

	if(data.isAdmin){
		return `Access Denied.`
	} else	{

		return User.findById({_id: data.id}).then( result =>{
			if(result == null){
				return `Something went wrong.`
			} else {
				return result.userOrders
			}
		})
	}
}