const User = require("../models/user");
const Product = require("../models/product");
const bcrypt = require("bcrypt");
const auth = require("../auth");


// Registration Code
module.exports.userRegistration = (reqBody) => {

	let newUser = new User ({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		password: bcrypt.hashSync(reqBody.password, 10)
	})

	return User.find().then(results => {
		
		const email = newUser.email;
		const result = results.find(e => e.email === email);

		if(result === undefined){ 

			return newUser.save().then((user, error) => {

				if(error) {
					return false
				} else {
					return true
				}
			})

		} else { 
			return false

		}

	})

	
}


// Login Code
module.exports.loginUser = (reqBody) => {

	return User.findOne({email: reqBody.email}).then(result => {

		if(result == null) {
			return false
		} else {

			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			if (isPasswordCorrect) {
				
				return {access: auth.createAccessToken(result)}
			}

			else {
				return false
			}
		}
	})
}


//Setting user to admin code

module.exports.changeToAdmin = async (reqParams, data) => {	

		if(data.isAdmin){

			return User.findById({_id: reqParams.userId}).then((result) => {

				if(result == null) {
					return false
				} else {
					result.isAdmin = true;
					return result.save().then(saveResult => {
						return true
					})
			}

		}) 
	
	} else { 

		return false
	}
			
}



module.exports.getProfile = (data) => {
console.log(data)
			return User.findById(data.userId).then(result => {
			
				result.password = "";

				
				return result;

			});

		};

