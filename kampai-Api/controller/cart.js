const Cart = require("../models/cart");
const User = require("../models/user");
const auth = require("../auth");
const Product = require("../models/product");

module.exports.createOrder = async (data, reqBody) => {

	if(data.isAdmin){

		return false
	} else {

		let newCart = new Cart({

			cartedOrder: reqBody.cartedOrder,
			total: reqBody.total
		});

		return newCart.save().then(result => {

			if(result == null){
				return false
			} else {
				return true
			}
		})

		}
	
}


module.exports.checkoutOrder = async (data) => {

	let isUserUpdated = await User.findById({_id: data.userId}).then(foundUser => {

		foundUser.userOrders.push({orderId:data.orderId});

		return foundUser.save().then(savedUser => {
			if(savedUser == null){
				return false
			} else {
				return true
			}
		})
	})

	let isProductUpdated = await Order.findById({_id: data.orderId}).then(foundOrder => {
		let  cartedOrder = foundOrder.cartedOrder;
		let soldProduct = [];
		soldProduct = cartedOrder.map(e => e.productId);

		for(let i = 0; i < soldProduct.length; i++){
			let productQty = [];
			productQty = cartedOrder.map(e => e.qty)

			for(let j = 0; j < productQty.length; j++){

				Product.findById({_id: product[i]}).then(foundProduct => {
				foundProduct.quantity = foundProduct.quantity - productQty[j];

				return foundProduct.save().then(saveProduct => {

					if(saveProduct == null) {
						return false
					} else {
						return true
					}

				})


			})



			}


			
		}

	})


	if (isProductUpdated == true && isProductUpdated == true){

		return true
	} else {
		return false
	}
}

// retrieve user's orders

module.exports.retrieveMyOrders = async (data) => {

	if(data.isAdmin){
		return false
	} else	{

		return User.findById({_id: data.id}).then( result =>{
			if(result == null){
				return false
			} else {
				return result.userOrders
			}
		})
	}
}
