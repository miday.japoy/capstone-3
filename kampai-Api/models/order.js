const mongoose = require("mongoose");


const orderSchema = new mongoose.Schema({

	totalAmount : {
		type : Number,
		required : [true, "Total is required."]
	},

	purchaseOn : {
		type : Date,
		default : new Date()
	},

	orderDetails : [

		{
			productId :{
				type : String,
				required : [true, "Product is required."]
			},

			qty : {
				type : Number,
				required : [true, "Quantity is required."]
			}
		}		

	]

})





module.exports = mongoose.model("Order", orderSchema);