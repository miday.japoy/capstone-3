const mongoose = require("mongoose");

const cartSchema = new mongoose.Schema({

	cartedOrder: [

		{
			_id : {
				type: String,
				required: [true, "Id is required."],
			},

			name : {
				type: String,
				required: [true, "Name is required."],
			},


			qty : {
				type : Number,
				required : [true, "Quantity is required."]
			},

			price: {
				type : Number,
				required : [true, "Price is required."]
			}
		}

	],

	total: {
				type: Number,
				required : [true, "Total is required."]
	}
		

})


module.exports = mongoose.model("Cart", cartSchema)